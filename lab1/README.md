## Operating Systems Lab 1
### Fork() and Pid() on Ubuntu in C

### Step 1: Hello World
Experience the world of c-programming as the early computer scientists did before the wimpy world of GUI's and integrated development systems.
Use Google to find a sample helloWorld program. The hardcore people use the vi pronounced vee eye.
http://www.cs.colostate.edu/helpdocs/vi.html
gcc is the compiler that you use to compile the program. http://ce.uml.edu/compile.htm
You should be able to find more compete instructions on the web.

### Step 2: fork() System Calls
Create a program for your Ubuntu system that uses the fork() and pid() system calls. There is a simple example in our text and a lot of them on the web. Find a simple one, get it to work, and then play with it so it does something cool! http://www.csl.mtu.edu/cs4411.ck/www/NOTES/process/fork/create.html 
