#include <stdio.h>
#include <unistd.h>

/**
  * Author: Abby Horner
  * Course: WOU CS365
  * Base code sourced from: https://www.thegeekstuff.com/2012/05/c-fork-function
  */
  
int global_var = 0;

int main() {
	int local_var = 0;
	printf("Pre-fork: local_var = %d, global_var = %d\n\n", local_var, global_var);
	
	// fork it
	int pid = fork();
	if (pid >= 0) // fork was successful
	{
		if (pid == 0) { // child process
			local_var++;
			global_var++;
			printf("\nCHILD PROCESS\nID = %d\nlocal_var = [%d]\nglobal_var = [%d]\n\n", getpid(), local_var, global_var);
		}
		else { // parent process
			local_var += 10;
			global_var += 20;
			printf("\nPARENT PROCESS\nID = %d\nlocal_var = [%d]\nglobal_var = [%d]\n\n", getppid(), local_var, global_var);
		}
	}
	else // fork failed
	{
		printf("\n Fork failed, quitting!!!\n");
		return 1;
	}
	return 0;
}