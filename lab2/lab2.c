#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

char* readBuf(void);
void parse(char* line, char** args);
void printPrompt(void);
void execute(char** args);
void showHelp(void);
void listJobs(void);

int main(int argc, char** argv) {
	int status;
	char* line;
	char* args[100];

	while(1) {
		printPrompt();
		line = readBuf();
		parse(line, args);
		if (strcmp(args[0], "exit") == 0) exit(0);
		else if (strcmp(args[0], "help") == 0) showHelp();
		else execute(args);
		free(line);
		free(args[0]);
	}
	return 0;
}

void showHelp() {
	puts("======HELP======"
	     "\nshell by Abby Horner"
	     "\ncommands that work:"
	     "\n> ls"
	     "\n> emacs"
	     "\n> gedit"
	     "\n> help"
	     "\n> exit"
	     "\n \tetc....");
}

char* readBuf() {
	char* line = malloc(sizeof(char)*100);
	fgets(line, 100, stdin);
	return line;
}

void parse(char* line, char** args) {
	int i = 0;
	char* token;
	const char delim[2] = " \n";
	
	token = strtok(line, delim);
	while (token != NULL && token != "\n") {
		args[i] = strdup(token);
		i++;
		token = strtok(NULL, delim);
	}
	args[i] = NULL;
}

void printPrompt(void) {
	printf("user@root: > ");
}

void execute(char** args) {
	pid_t pid = fork();
	
	if (pid == 0) {
		if(execvp(args[0], args) < 0) {
			printf("ERROR: couldn't execute command!\n");
			printf("TRIED: %s\n", args[0]);
			exit(1);
		}
		exit(0);
	}
	else if (pid < 0) {
		printf("ERROR: forking failed\n");
		exit(1);
	}
	else {
		wait(NULL);
		return;
	}
	
}
