## Lab 2: Write your own shell in C

In this lab, you will write a simple shell by your own. In this shell, it takes an input command, and then executes it. Your shell should implement the following:  

* Take external commands, such as emacs, ls, and so on
+ Take internal commands
    * __exit__: terminate the shell
    * __help__: help information of your shell